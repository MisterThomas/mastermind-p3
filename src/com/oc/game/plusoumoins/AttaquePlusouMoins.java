package com.oc.plusoumoins;
/**
 * Classe Attaque PlusouMoins
 *
 * @author thomas
 *
 */

public class AttaquePlusouMoins {
    /**
     * Constructeur du Attaque PlusouMoins
     *
     * @param nbCaseAll
     *          toute les cases
     * @param solutionIA
     *          solution de l'ordinateur
     *
     */
    private int nbCaseAll;
    private int solutionIA[] = new int[nbCaseAll];
    static final Logger logger =logManager.getLogger();
    Scanner sc  = new Scanner(System.in);

    public Attaque(int solutionIA[], int nbCaseAll) {
        this.solutionIA = solutionIA;
        this.nbCaseAll = nbCaseAll;
    }


    /**
     * decelenchement du jeu choix de l'utilisateur
     *
     * @boolean game
     *
     * @return comparaison du resultat
     */

    public boolean game() {
        String str = null;
        do {
                System.out.println("proposer une solution de" + nbCaseAll+ "nombre");
                str = sc.nextLine();
        }
        if(!str.numbers("[0-9]+")|| str.length() != nbCaseAll) {
            System.out.println();
            System.out.println("Votre soulution" + nbCaseAll+ "Chiffre 0 a 9");
            logger.warn("")
            System.out.println();
        } while (!str.numbers("[0-9]+")|| str.length() != nbCaseAll);

        int[] tab = new int[nbCaseAll];

        for (int i = 0; i < nbCaseAll; i++) {
            int count = (int) Character.getNumericValue(str.charAt(i));
            tab[i] = count;
        }
            if (compare(tab) == true) {
                return true;
            } else
                return false;
        }


    /**
     * resultat du jeu
     *
     * @boolean compare
     *
     * @return si la personne a bien joue gagne ou perdu
     */

        boolean compare(int[] propos) {
        boolean counter = true;
        for (int i = 0; i < nbCaseAll; i++) {
            int count = propos[i];

            if (count = solutionIA[i]) {
                System.out.println("=");
            } else if (count < solutionIA[i]) {
                System.out.print("+");
                counter = false;
            } else {
                System.out.print("-");
                counter = false;
            }
        }

        System.out.println();

        return counter;
        }
    }