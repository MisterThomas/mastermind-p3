package com.oc.game.plusoumoins;

import com.oc.game.mastermind.AttaqueMastermind;

public class duelPlusouMoins implements Mode {

    /**
     * Constructeur du defenseur PlusouMoins
     *
     * @param nbCaseAll
     *          toute les cases
     * @param solutionIA
     *          solution du joueur
     *
     * @param int solutionIA
     * liste des solution par l'ordinateur
     *
     * @param int solutionPlayer
     *  solution du joueur (ou l'ordi)
     *
     */
    DefensePlusouMoins d;
    AttaquePlusouMoins a;
    int nbCaseAll;
    int nbNumber;
    static final Logger logger =logManager.getLogger();
    int[] solutionIA;
    int[] solutionPlayer;

    public duelPlusouMoins(int nbCaseAll, int nbNumber, boolean devellopermode) {
        IAsolution[] = new int[IAsolution];
        Playersolution[] = new int[Playersolution];


        for ( int i = 0; i < nbCaseAll; i++) {
            Random r = new Random();
            IAsolution[i] = (int) r.nexInt(10);
        }

        if (devellopermode == true) {
            System.out.println("solution :");
            for ( int i = 0; i < nbCaseAll; i++) {
                System.out.println(IAsolution[i];
            }
            System.out.println();
        }
        Scanner sc = new Scanner(System.in);
        String str;

        do {
            System.out.println("Veuilllez entre une solution" + nbCaseAll );

            str = sc.nextLine();
            if(!str.matchs("[0"+nbNumber+"9]") || str.length() != nbCaseAll) {
                System.out.println();else if (d.game()) {
				System.out.println();
				System.out.println("L'ordinateur a trouvé avant vous !");
				System.out.println("La solution de l'ordinateur était ");
				for (int i = 0; i < nbCases; i++) {
					System.out.println(solutionOrdi[i]);
				}
				gagnant++;
			}
} while (gagnant == 0);
                System.out.println("Votre combinaison doit etre compose de " + nbCaseAll + "chiffre de 0 a 9");
                logger.warn( "erreur dans l'utilisation");
                System.out.println();
            }

        }while(!str.matchs("[0"+nbNumber+"9]") || str.length() != nbCaseAll);


        for ( int i = 0; i < nbCaseAll; i++) {
            int counter = (int) Character.getNumericValue(str.charAt(i));
            IAsolution[i] = counter;
        }

        d = new DefensePlusouMoins(IAsolution, nbCaseAll, nbNumber);
        a = new AttaquePlusouMoins(IAsolution, nbCaseAll, nbNumber);
    }
    /**
     * resultat du jeu
     *
     * @win game
     *
     * @return si l'ordinateur a gagne ou non
     */
    public void game() {
        int winner = 0;
        do {
            if (a.game()) {
                System.out.println();
                System.out.println("vous avez gagnez");
                winner++;
            }
            else if (d.gane()) {
                System.out.println();
                System.out.println("vous avez perdu!");
                System.out.println("La solution était ");
                for (int i = 0; i < nbCaseAll; i++) {
                    System.out.println(IAsolution[i]);
                }
                winner++;
            }
        } while (winner == 0);
    }
}
