package com.oc.game.plusoumoins;


/**
 *Classe Defense PlusouMoins
 *
 * @author thomas
 *
 */

public class defenseurPlusouMoins implements Mode {

    /**
     * Constructeur du defenseur PlusouMoins
     *
     * @param nbCaseAll
     *          toute les cases
     * @param solutionIA
     *          solution du joueur
     *
     * @param nbtest
     *
     * liste des tests propostions + -
     */
    DefensePlusouMoins d;
    int nbTest;
    int nbCaseAll;
    static final Logger logger =logManager.getLogger();

    public challengerMastermind(int nbCaseAll, int nbTest) {
        this.nbTest = nbTest;
        this.nbCaseAll = nbCaseAll;
        int solutionIA[] = new int[solutionIA];

        Scanner sc = new Scanner(System.in);
        String str;
        do {
            System.out.println("Veuilllez entre une solution" + nbCaseAll );

            str = sc.nextLine();
            if(!str.matchs("[0 9]") || str.length() != nbCaseAll) {
                System.out.println();
                System.out.println("Votre combinaison doit etre compose de " + nbCaseAll + "chiffre de 0 a 9");
                logger.warn( "erreur dans l'utilisation");
                System.out.println();
            }

        }while(!str.matchs("[0 9]") || str.length() != nbCaseAll);


        for ( int i = 0; i < nbCaseAll; i++) {
            int counter = (int) Character.getNumericValue(str.charAt(i));
            solutionIA[i] = counter;
        }

        d = new defenseurPlusouMoins(solutionIA, nbCaseAll);
    }
    /**
     * donne le resultat
     *
     * @void game
     *donne le resultat si gagner ou non
     */
    public void game() {
        boolean test = false;
        int IA = 0;
        do {
            test = d.game();
            IA++;
        } while (test == false && IA != nbCaseAll);
        System.out.println();
        if (test = true) {
            System.out.println();
            System.out.println("la bonne reponse etait");
        } else {
            System.out.println();
            System.out.println(" la reponse etait");
        }
        System.out.println();
    }
}
