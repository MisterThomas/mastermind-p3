package com.oc.game.mastermind;
/**
 * Classe Attaque Mastermind
 *
 * @author thomas
 *
 */
public class challengerMastermind  implements Mode {
    /**
     * Constructeur du challengerMastermind
     *
     * @param nbCaseAll
     *          toute les cases
     * @param solutionIA
     *          solution de l'ordinateur
     * @param  nbtest
     *          test de proposition
     *
     */
        AttaqueMastermind a;
        int nbTest;
        int nbCaseAll;
        static final Logger logger =logManager.getLogger();
        int solutionIA[];

        public challengerMastermind(int nbCaseAll, int nbNumber, int nbTest, boolean devellopermode) {
            /**
             * Constructeur du challenger Mastermind
             *
             * @param nbCaseAll
             *          toute les cases
             * @param solutionIA
             *          solution de l'ordinateur
             * @param  nbNumber
             *          Numero de 0 a 9
             *
             * @param test
             *
             * test de proposition
             */

            this.nbTest = nbTest;
            this.nbCaseAll = nbCaseAll;
            IAsolution = new int[IAsolution];

            for ( int i = 0; i < nbCaseAll; i++) {
                random r = new random ();
                IAsolution[i] = (int) r.nextInt(nbNumber + 1);
            }

            if (devellopermode == true) {
                System.out.println("solution :");
                for (int i = 0; i < nbCaseAll; i++) {
                    System.out.print(solutionIA[i]);
                }
                System.out.println();
            }

            a = new AttaqueMastermind(solutionIA, nbCaseAll, nbNumber)
        }

        public void game() {

            /**
             * donne le resultat
             *
             * @boolean test
             *
             * donne le resultat si le joueur a gagne ou non
             */
            boolean test = false;
            int IA = 0;
            do {
                test = a.test();
                computeur++;
            } while (test == false && computeur != nbCaseAll);
            System.out.println();
            if (test = true) {
                System.out.println();
            } else {
                System.out.println( "Vpus avez perdu");
                System.out.println(" la reponse etait");
                for (int i = 0; i < nbCaseAll; i++) {
                    System.out.print(solutionIA[i])
                }
            }
            System.out.println();
        }
}
