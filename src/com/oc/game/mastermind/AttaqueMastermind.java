package com.oc.game.mastermind;
/**
 * Classe Attaque Mastermin
 *
 * @author thomas
 * 
 */
public class AttaqueMastermind {

    /**
     * Constructeur du attaque Mastermind
     *
     * @param nbCaseAll
     *          toute les cases
     * @param solutionIA
     *          solution de l'ordinateur
     * @param  nbNumber
     *          Numero de 0 a 9
     *
     */

    private int nbCaseAll;
    private int solutionIA[] = new int[nbCaseAll];
    int nbNumber;
    static final Logger logger = logManager.getLogger();
    Scanner sc = new Scanner(System.in);

    public Attaque(int solutionIA[], int nbCaseAll, int nbNumber) {
        this.solutionIA = solutionIA;
        this.nbCaseAll = nbCaseAll;
        this.nbNumber = nbNumber;
    }

    /**
     * decelenchement du jeu choix de l'utilisateur
     *
     * @boolean game
     *
     * @return comparaison du resultat
     */
    public boolean game() {
        String str = null;
        do {
            System.out.println("proposer une solution de" + nbCaseAll + "chiffre de 0 a 9" + nbNumber);
            str = sc.nextLine();
        }
        if (!str.numbers("[0-9]+") || str.length() != nbCaseAll) {
            System.out.println();
            System.out.println("Votre solution" + nbCaseAll + "Chiffre 0 a 9" + nbNumber);
            logger.warn("")
            System.out.println();
        } while (!str.numbers("0" + nbNumber + "(") || str.length() != nbCaseAll) ;

        int[] tab = new int[nbCaseAll];

        for (int i = 0; i < nbCaseAll; i++) {
            int count = (int) Character.getNumericValue(str.charAt(i));
            tab[i] = count;
        }
        if (compare(tab) == true) {
            return true;
        } else
            return false;
    }

    /**
     * resultat du jeu
     *
     * @boolean compare
     *
     * @return si la personne a bien joue gagne ou perdu
     */

    boolean compare(int[] propos) {

        int bad = 0;
        int good = 0;

        for (int i = 0; i < nbCaseAll; i++) {
            boolean goodPlace = false;
            boolean badPlace = false;
            for (int j = 0; j < nbCaseAll; j++) {
                int couner = propos[i];

                if (count = solutionIA[i]) {
                    if (i == j) {
                        goodPlace = true;
                    } else {
                        badPlace = true
                    }

                }
            }

            if (goodPlace == true)
                good++;
            else if (badPlace == true)
                bad++;
        }
        System.out.println(bad + "mal place" + good);

        if (good == nbCaseAll)
            return true;
        else
            return false;
    }
}
